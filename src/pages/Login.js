
// impory the react bootstrap script
import {useState , useEffect} from 'react';


import { Form,Button } from "react-bootstrap";


export default function Login() {
    const[email, setEmail] = useState("")
     const [password, setpasword] = useState("");

    
          //determine whether submit  button is enabled or not
     const [isActive, setIsActive] = useState(false)

            useEffect(() => {   
                // validation to enabled the sumbit button when all the fields are populated and both password match
                if ((email !== '' ) &&  (password === password)){
                    setIsActive(true);
                }else {
                    setIsActive(false);
                }
            }, [email, password])


            function loginUser(e){
                e.preventDefault();
    
                setEmail('');
                setpasword('');
           
            
                    alert("thank you for regiter")
    
            }



    
    return(
        <>
              <h1>Log in</h1>
        
         <Form onSubmit={e => loginUser(e)}>    
                   <Form.Group>
                               <Form.Label>Email Address</Form.Label>
                               <Form.Control  type ="Email" placeholder="Enter your email address" required 
                                   value = {email} onChange = {e => setEmail(e.target.value)}/>
                               <Form.Text className="text-muted">
                                       We' ll never share emails with others
                               </Form.Text>
                   </Form.Group>
                   
                   <Form.Group controlId = "password1">
                               <Form.Label>password</Form.Label>
                               <Form.Control type = "password" placeholder = "Enter your password" required   value = {password} onChange = {e => setpasword(e.target.value)}/>
                   </Form.Group>

                {isActive ? 
                  <Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">
                  Register
                </Button>
  
                   :
                   <Button className ="mt-3 mb-5" variant ="danger" type = "submit" id ="submitBtn" disabled>Register</Button>

                   }
         </Form>  
      </>

   )
     




}