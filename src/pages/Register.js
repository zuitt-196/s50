// everytime we create an page make sure you indicate the export 
// importing the hooks
import {useState , useEffect} from 'react';


import { Form,Button } from "react-bootstrap";

export default function Register(){
    const[email, setEmail] = useState("")
     const [password1, setpasword1] = useState("");
     const [password2, setpasword2] = useState("");
    
     //determine whether submit  button is enabled or not
     const [isActive, setIsActive] = useState(false)


        //  check if values are successfully binded
            console.log(email);
            console.log(password1);
            console.log(password2);
            //Syntax
                    // useEffect (() => {}, [])

        function registerUser(e){
            e.preventDefault();

            setEmail('');
            setpasword1('');
            setpasword2('');
        
                alert("thank you for regiter")

        }
        // to make validation to an apllicattion used by useEffect
            useEffect(() => {   
                // validation to enabled the sumbit button when all the fields are populated and both password match
                if ((email !== '' && password1 !== '' && password2 !== '') &&  (password1 === password2)){
                    setIsActive(true);
                }else {
                    setIsActive(false);
                }
            }, [email, password1, password2])
        return(
             <>
                   <h1>Register Here:</h1>
             
              <Form onSubmit={e => registerUser(e)}>    
                        <Form.Group>
                                    <Form.Label>Email Address</Form.Label>
                                    <Form.Control  type ="Email" placeholder="Enter your email address" required 
                                        value = {email} onChange = {e => setEmail(e.target.value)}/>
                                    <Form.Text className="text-muted">
                                            We' ll never share emails with others
                                    </Form.Text>
                        </Form.Group>
                        
                        <Form.Group controlId = "password1">
                                    <Form.Label>password</Form.Label>
                                    <Form.Control type = "password" placeholder = "Enter your password" required   value = {password1} onChange = {e => setpasword1(e.target.value)}/>
                        </Form.Group>

                        <Form.Group controlId = "password2">
                                    <Form.Label>Verify password</Form.Label>
                                    <Form.Control type = "password" placeholder = "Enter your password" required  value = {password2} onChange = {e => setpasword2(e.target.value)}/>
                        </Form.Group>
                     {isActive ?      
                         <Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">  Registe</Button>
                      :
                        <Button className ="mt-3 mb-5" variant ="danger" type = "submit" id ="submitBtn" disabled>Register</Button>

                        }
              </Form>  
           </>

        )

}